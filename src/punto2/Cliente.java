/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package punto2;

/**
 *
 * @author Horacio Chazarreta
 */
public class Cliente {

    private String dni;
    private String ayn;
    private String telefono;

    public Cliente(String dni, String ayn, String telefono) {
        this.dni = dni;
        this.ayn = ayn;
        this.telefono = telefono;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getAyn() {
        return ayn;
    }

    public void setAyn(String ayn) {
        this.ayn = ayn;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String detallesCliente() {
        return "\nDatos del Cliente" + "\nDNI: " + dni
                + "\nApellido y Nombre: " + ayn 
                + "\nTeléfono: " + telefono;
    }

    @Override
    public String toString() {
        return "Cliente{" + "dni=" + dni + ", ayn=" + ayn + ", telefono=" + telefono + '}';
    }

}
