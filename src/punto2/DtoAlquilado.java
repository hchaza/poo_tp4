/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package punto2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Horacio Chazarreta
 */
public class DtoAlquilado {

    private Cliente cliente;
    private Departamento departamento;
    private LocalDate fecha;
    private int cantidadDias;

    public DtoAlquilado() {
    }

    public DtoAlquilado(Cliente cliente, Departamento departamento, LocalDate fecha, int cantidadDias) {
        this.cliente = cliente;
        this.departamento = departamento;
        this.fecha = fecha;
        this.cantidadDias = cantidadDias;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public int getCantidadDias() {
        return cantidadDias;
    }

    public void setCantidadDias(int cantidadDias) {
        this.cantidadDias = cantidadDias;
    }

    public String verDtoAlquilado() {
        return "\nDatos del Departamento Alquilado"
                + "\nEl departamento Código " + departamento.getCodigo()
                + " está alquilado a " + cliente.getAyn()
                + " desde el día " + fecha.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                + " por " + cantidadDias + " días.";
    }

    public LocalDate calcularFecha() {
        LocalDate newDate = this.fecha.plusDays(cantidadDias);
        //newDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return newDate;
    }

}
