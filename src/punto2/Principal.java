/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package punto2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Horacio Chazarreta
 */
public class Principal {

    public static void main(String[] args) {

        // Instanciar 1 Cliente y 1 Departamento con datos a eleccion (disponibilidad en TRUE)
        Cliente cli = new Cliente("34566780", "Pérez Juan", "3885 409-3545");
        Departamento dpto = new Departamento(1, "Departamento amoblado", "Jujuy 345", 20000, true);

        // Ejecutar el metodo detallesCliente
        System.out.println(cli.detallesCliente());

        // Ejecutar el método detallesDto
        System.out.println(dpto.detallesDto());

        // Cambiar el valor del atributo disponible del departamento a ocupado
        dpto.setDisponibilidad(false);

        // Crear instancia de DtoAlquilado asignando el cliente y departamento
        DtoAlquilado dptoAlq = new DtoAlquilado(cli, dpto, LocalDate.of(2020, 5, 31), 2);
        //DateTimeFormatter f = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        //dptoAlq.getFecha().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        
        /*
        Scanner entrada = new Scanner(System.in);
        DtoAlquilado dptoAlq1 = new DtoAlquilado();
        System.out.println("Ingrese la Fecha:");
        dptoAlq1.setFecha(LocalDate.parse(entrada.next()));
        System.out.println(dptoAlq1.verDtoAlquilado());
         */
        
        // Ejecutar el método verDtoAlquilado
        System.out.println(dptoAlq.verDtoAlquilado());

        // Ejecutar el método detallesDto
        System.out.println(dpto.detallesDto());

        // Mostrar la fecha de devolución del departamento
        System.out.println("\nLa fecha de devolución del departamento es: "
                + dptoAlq.calcularFecha().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
    }
}
