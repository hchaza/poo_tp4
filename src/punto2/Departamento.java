/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package punto2;

/**
 *
 * @author Horacio Chazarreta
 */
public class Departamento {

    private int codigo;
    private String descripcion;
    private String ubicacion;
    private float precio;
    private boolean disponibilidad;

    public Departamento(int codigo, String descripcion, String ubicacion, float precio, boolean disponibilidad) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.ubicacion = ubicacion;
        this.precio = precio;
        this.disponibilidad = disponibilidad;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public boolean isDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public String verDisponibilidad() {
        String disp = (this.disponibilidad == true) ? "Si está disponible"
                : "No está disponible";
        return disp;
    }

    public String detallesDto() {
        return "\nDatos del Departamento" + "\nCóodigo: " + codigo 
                + "\nDescripción: " + descripcion 
                + "\nUbicación: " + ubicacion 
                + "\nPrecio: " + precio 
                + "\nDisponibilidad: " + verDisponibilidad();
    }

    @Override
    public String toString() {
        return "Departamento{" + "codigo=" + codigo + ", descripcion=" + descripcion + ", ubicacion=" + ubicacion + ", precio=" + precio + ", disponibilidad=" + disponibilidad + '}';
    }
    
}
